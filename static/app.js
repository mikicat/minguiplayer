
function session_close() {
    //cosas
}

var socket = io.connect('http://' + document.domain + ':' + location.port);
socket.on('connect', function() {
    socket.emit('client_connected', {data: 'New client!'});
});

socket.on('session_close', function (data) {
    //document.getElementById("ses_id").innerHTML = "Connection closed!";
    //document.getElementById("qr_image").remove();
    location.reload();
});

socket.on('queue_update', function(data) {
    console.log("Queue Update!");
    document.getElementById("queue").contentDocument.location.reload(true);
})
