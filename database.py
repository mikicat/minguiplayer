import sqlite3
import os

class Database:
    def __init__(self):
        isNew = not os.path.isfile("db/database.db")
        self.connection = sqlite3.connect("db/database.db")
        if isNew: self.create()

    def create(self):
        cursor = self.connection.cursor()
        cursor.execute("CREATE TABLE data (id TEXT PRIMARY KEY, img_path TEXT UNIQUE NOT NULL);")
        cursor.execute("CREATE TABLE devices (session_id, device_id PRIMARY KEY, isHost NUMBER(1), isAdmin NUMBER(1), FOREIGN KEY (session_id) REFERENCES data(id), CONSTRAINT check_isHost CHECK (isHost IN (0,1)), CONSTRAINT check_isAdmin CHECK (isAdmin IN (0,1)) )")
        cursor.execute("CREATE TABLE queue (id TEXT, tracklist_number INTEGER PRIMARY KEY, source TEXT NOT NULL, url TEXT NOT NULL, FOREIGN KEY (id) REFERENCES data (id) )")
        self.connection.commit()

    def add_session(self, session_id, path, device_id):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO data VALUES(?, ?)", (session_id, path))
        cursor.execute("INSERT INTO devices VALUES(?, ?, ?, ?)", 
                (session_id, device_id, 1, 0))
        self.connection.commit()

    def get_session(self, id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM data WHERE id = ?", (id,))
        return cursor.fetchone()

    def get_all(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM data") 
        tot = cursor.fetchall()
        cursor.execute("SELECT * FROM queue")
        tot.append(cursor.fetchall())
        cursor.execute("SELECT * FROM devices")
        tot.append(cursor.fetchall())
        return tot

    def delete_session(self, id):
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM queue WHERE id = ?", (id,))
        cursor.execute("DELETE FROM devices WHERE session_id = ?", (id,))
        cursor.execute("DELETE FROM data WHERE id = ?", (id,))
        self.connection.commit()

    def get_devices(self, session_id):
        cursor = self.connection.cursor()
        cursor.execute ("SELECT * FROM devices WHERE session_id = ?", (session_id,))
        return cursor.fetchall()
    
    def add_device(self, session_id, device_id):
        cursor = self.connection.cursor()
        if len(self.get_devices(session_id)) > 1:
            cursor.execute("INSERT INTO devices VALUES (?, ?, ?, ?)",
                    (session_id, device_id, 0, 0))
        else:
            cursor.execute("INSERT INTO devices VALUES (?, ?, ?, ?)",
                    (session_id, device_id, 0, 1))
        self.connection.commit()

    def delete_device(self, device_id):
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM devices WHERE device_id = ?", (device_id,))
        self.connection.commit()
        
    def get_device(self, device_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM devices WHERE device_id = ?", (device_id,))
        return cursor.fetchone() # Només hi ha d'haver 1 sol resultat (device_id és clau primària)

    def make_admin(self, device_id):
        cursor = self.connection.cursor()
        cursor.execute("UPDATE devices SET isAdmin = 1 WHERE device_id = ?", (device_id,))

    def add_song_to_queue(self, session_id, source, url):
        cursor = self.connection.cursor()
        cursor.execute()
        cursor.execute("INSERT INTO queue VALUES (?, ?, ?)",
                       (session_id, source, url))
        self.connection.commit()

    def get_first_song(self, session_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM queue WHERE id = ? ORDER BY tracklist_number ASC LIMIT 1", (session_id,))
        return cursor.fetchone()

    def delete_first_song(self, session_id):
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM queue WHERE id = ? ORDER BY tracklist_number ASC LIMIT 1", (session_id,))
        self.connection.commit()

    def close(self):
        self.connection.close()

if __name__ == '__main__':
    d = Database()
    print(d.get_all())
    d.close()
