import os
import random
from Exceptions.SessionExceptions import *
from database import *
from manager import *
import qrcode
LENGTH_ID = 10

class Session:
    def __init__(self):
        self.exists = False
        self.db = Database()
        self.mg = Manager()

    def create(self, url_root):
        self.exists = True
        self.id = self.__rand_name(LENGTH_ID)
        url = url_root + 'join/' + self.id
        self.img_name,self.img_path = self.__gen_qr(url)
        # Crear a DB
        cookie = self.__gen_cookie()
        self.db.add_session(self.id, self.img_name, cookie)
        self.mg.add_manager(self.id)
        return cookie

    def destroy(self):
        # Eliminar de DB
        os.remove('static/'+self.getImgPath())
        self.db.delete_session(self.id)
        self.mg.delete_manager(self.id)
        self.exists = False

    def it_exists(self, id):
        self.id = id
        try:
            if len(self.db.get_session(self.id)) != 2: return False
        except TypeError:
            return False
        self.exists = True
        return True

    def join(self):
        cookie = self.__gen_cookie()
        self.db.add_device(self.id, cookie)
        return cookie

    def leave(self, device_id):
        self.exists = True
        dev = self.db.get_device(device_id)
        if not dev: 
            self.exists = False
            return self.exists
        # tupla: (session_id, device_id, isHost, isAdmin)
        if dev[3] == 1: # esAdmin
            devices = self.db.get_devices(dev[0])
            if len(devices) > 2: # Un és ell i l'altre el Host
                for d in devices:
                    if d[1] != dev[1] and d[2] != 1:
                        self.db.make_admin(d[1])
                        break
            else:
                self.id = dev[0]
                self.destroy()
                return True
        self.db.delete_device(device_id)
        self.exists = False
        return True

    def getId(self):
        if not self.exists: raise SessionNotCreated()
        return self.id
    
    def getImgPath(self):
        if not self.exists: raise SessionNotCreated()
        info = self.db.get_session(self.id)
        # info és una tupla: (id, img_path)
        self.img_path = 'images/'+info[1]+'.png'
        return self.img_path

    def getDevice(self, device_id):
        dev = self.db.get_device(device_id)
        if dev and len(dev) > 0: 
            self.exists = True
            self.id = dev[0] # inicialitzem self.id amb session_id
            return dev
        else:
            self.exists = False
            return self.exists

    def __gen_cookie(self):
        cookie = self.__rand_name(30)
        return cookie

    def __rand_name(self, strlen):
        random_string = ''
        for _ in range(strlen):
            # Considering only upper and lowercase letters
            random_integer = random.randint(97, 97 + 26 - 1)
            flip_bit = random.randint(0, 1)
            # Convert to lowercase if the flip bit is on
            random_integer = random_integer - 32 if flip_bit == 1 else random_integer
            # Keep appending random characters using chr(x)
            random_string += (chr(random_integer))
        return random_string

    def __gen_qr(self,data):
        img = qrcode.make(data)
        random_name = self.__rand_name(LENGTH_ID)
        img_file = random_name+'.png'
        img_path = 'images/'+img_file
        img.save('static/'+img_path)
        return random_name,img_path
