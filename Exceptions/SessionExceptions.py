class SessionNotCreated(Exception):
    """Exception raised for sessions which have not been created yet
    """

    def __init__(self, message="This session has not been created yet"):
        self.message = message
        super().__init__(self.message)
