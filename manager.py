from database import *
from streamer import *
import sqlite3


class Manager:
    def __init__(self):
        self.db = Database()
        self.st = Streamer()

        isNew = not os.path.isfile("db/manager.db")
        self.connection = sqlite3.connect("db/manager.db")
        if isNew: self.create()

    def create_manager_table(self):
        cursor = self.connection.cursor()
        cursor.execute("CREATE TABLE managers (session_id TEXT PRIMARY KEY, playing INTEGER NOT NULL);")
        self.connection.commit()

    def delete_manager(self, session_id):
        cursor = self.connection.cursor()
        cursor.execute("DELETE FROM managers WHERE session_id = ?", (session_id,))
        self.connection.commit()

    def add_manager(self, session_id):
        cursor = self.connection.cursor()
        cursor.execute("INSERT INTO data VALUES(?, ?)", (session_id, 0))
        self.connection.commit()

    def is_playing(self, session_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT playing FROM managers WHERE session_id = ?", (session_id,))
        return cursor.fetchone()['playing']

    def change_playing(self, new_value, session_id):
        cursor = self.connection.cursor()
        cursor.execute("UPDATE managers SET playing = ? WHERE session_id = ?", (new_value, session_id))

    def add_song_to_queue(self, session_id, source, url):
        self.db.add_song_to_queue(session_id, source, url)

    def next_song(self, session_id):
        if not self.is_playing():
            self.play_song(session_id)
        else:
            return

    def play_song(self, session_id):
        result = self.db.get_first_song(session_id)
        self.st.play_song(result['url'], result['source'])
        self.db.delete_first_song()
        self.next_song(session_id)
