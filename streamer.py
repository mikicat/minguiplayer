import pafy
import vlc


class Streamer:
    def __init__(self):
        self.title = None
        self.author = None

    def play_song(self, url, local):
        is_opening = False
        is_playing = False

        if not local:
            video = pafy.new(url)
            self.title = video.title
            self.author = video.author

            best = video.getbestaudio()
            play_url = best.url

        instance = vlc.Instance()
        player = instance.media_player_new()

        if not local:
            media = instance.media_new(play_url)
        else:
            media = instance.media_new(url)

        media.get_mrl()
        player.set_media(media)
        player.play()

        good_states = [
            "State.Playing",
            "State.NothingSpecial",
            "State.Opening"
        ]

        while str(player.get_state()) in good_states:
            if str(player.get_state()) == "State.Opening" and is_opening is False:
                print("Status: Loading")
                is_opening = True

            if str(player.get_state()) == "State.Playing" and is_playing is False:
                print("Status: Playing")
                is_playing = True

        print("Status: Finish")
        player.stop()
