from flask import *
import os
import requests
from werkzeug.utils import secure_filename
from Exceptions.SessionExceptions import *
from session import *
from manager import *
from flask_socketio import SocketIO
from flask_socketio import namespace, send, emit

UPLOAD_FOLDER = os.getcwd() + '/files'
ALLOWED_EXTENSIONS = {'mp3'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# Websockets:
# https://tutorials.technology/tutorials/61-Create-an-application-with-websockets-and-flask.html

app = Flask(__name__)
socketio = SocketIO(app)

@app.route('/index.html')
def trolling():
    return "We do a little bit of <i>trolling</i>..."


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/render')
def render():
    if not 'device_id' in request.cookies:
        s = Session()
        cookie = s.create(request.url_root)
        response = make_response(redirect('/session'))
        response.set_cookie('device_id', cookie)
        return response
    return redirect('/session')


@app.route('/join/<session_id>')
def join(session_id):
    if 'device_id' in request.cookies:
        return redirect('/session')
    s = Session()
    if s.it_exists(session_id):
        cookie = s.join()
        response = make_response(redirect('/session'))
        response.set_cookie('device_id', cookie)
    else:
        response = make_response(redirect('/fail'))
    return response


@app.route('/fail')
def fail():
    return render_template("fail.html")


@app.route('/session')
def serve_session():
    if 'device_id' not in request.cookies:
        return redirect('/')  # Gestionar despres ok
    cookie = request.cookies.get('device_id')
    s = Session()
    device = s.getDevice(request.cookies.get('device_id'))
    if not device:
        response = make_response(redirect('/'))
        response.delete_cookie('device_id')
        return response
    # tupla de l'estil (session_id, device_id, isHost, isAdmin)
    if (device[2] == 1):
        return render_template("qr.html", img_path=s.getImgPath(), session_id=s.getId())
    else:
        return render_template("client.html")


@app.route('/leave')
def leave():
    if not 'device_id' in request.cookies:
        return redirect('/')
    s = Session()
    s.leave(request.cookies.get('device_id'))
    response = make_response(redirect('/'))
    response.delete_cookie('device_id')
    return response


@app.route('/destroy')
def destroy():
    if not 'device_id' in request.cookies:
        return redirect('/')
    s = Session()
    if not s.getDevice(request.cookies.get('device_id')):
        return redirect('/fail')
    s.destroy()


@app.route('/add_song_to_queue')
def add_song_to_queue():
    url = request.args.get('url', None)
    session_id = request.args.get('session_id', None)
    if 'device_id' not in request.cookies or 'url' not in request.args.get() or 'session_id' not in request.args.get():
        return render_template('client.html')
    m = Manager()
    if url.find('https://www.youtube.com/'):
        m.add_song_to_queue(session_id, 0, url)
    else:
        m.add_song_to_queue(session_id, 1, url)
    return render_template('client.html')


@app.route('/post_mp3')
def post_mp3():
    file = request.args.get('file')
    session_id = request.args.get('session_id')
    if file is None:
        flash('No file part')
        return render_template('client.html')
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        requests.post('http://localhost:80/add_song_to_queue',
                      data={'url': os.path.join(app.config['UPLOAD_FOLDER'], filename),
                            'session_id': session_id})
        return render_template('client.html')

    return redirect("index.html")
    
@app.route('/queue')
def print_queue():
    print("hola")
    socketio.emit('queue_update', {'update':True})
    return "hola"

if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=80)
    socketio.run(app, host='0.0.0.0', port=80)


@socketio.on('client_connected')
def handle_client_connect_event(json):
    print('received json: {0}'.format(str(json)))

@socketio.on('session_close')
def handle_session_closure(json):
    pass
    # it will forward the json to all clients.
        # print('Message from client was {0}'.format(json))
        # emit('connection_close', 'Message from backend')

